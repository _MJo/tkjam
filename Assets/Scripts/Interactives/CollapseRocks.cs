﻿using System.Collections;
using System.Collections.Generic;
using ActionEvents;
using Managers;
using UnityEngine;

public class CollapseRocks : MonoBehaviour
{
    public List<GameObject> Rocks;

    public void CollapseRockList()
    {
        if (Rocks == null) return;
        foreach (var rock in Rocks)
        {
            rock.GetComponent<Rigidbody>().isKinematic = false;
            rock.AddComponent<DestroyTime>();
        }
    }
}
