﻿using System.Collections;
using System.Collections.Generic;
using ActionEvents;
using Managers;
using UnityEngine;

public class RockActivator : MonoBehaviour
{
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            RaycastHit hitInfo;
            Ray r = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (!Physics.Raycast(r, out hitInfo, 1000)) return;
            if (hitInfo.collider != gameObject.GetComponent<Collider>()) return;
            GetComponentInParent<CollapseRocks>().CollapseRockList();
        }
    }
}
