﻿using System;
using System.Collections;
using System.Collections.Generic;
using Managers;
using TravelersEvents;
using UnityEngine;

public class WindManager : MonoBehaviour
{
    public int Force;
    public float WindCooldown;
    public float WindDuration;

    private List<GameObject> travelers;
    private float windTimer;

    void Awake()
    {
        windTimer = WindCooldown;
        travelers = new List<GameObject>();
        EventManager.Instance.AddListener<RegisterTravelerEvent>(RegisterTraveler);
        EventManager.Instance.AddListener<RemoveTravelerEvent>(RemoveTraveler);

    }

    void Update()
    {
        windTimer = Mathf.MoveTowards(windTimer, WindCooldown, Time.deltaTime);
        if (windTimer < WindCooldown) return;
        //Debug.Log("Wind ready");
        if (Input.GetKeyDown(KeyCode.Q))
        {
            windTimer = 0;
            AddWind(-Force);
        }
        else if(Input.GetKeyDown(KeyCode.W))
        {
            windTimer = 0;
            AddWind(Force);
        }
    }

    private void AddWind(int force)
    {
        if (travelers == null) return;
        foreach (var traveler in travelers)
        {
            traveler.AddComponent<ConstantForce>().force = new Vector3(force,0,0);
            traveler.AddComponent<DestroyWind>().SetDestroy(WindDuration);
        }
    }

    private void RegisterTraveler(RegisterTravelerEvent e)
    {
        travelers.Add(e.Traveler);
    }

    private void RemoveTraveler(RemoveTravelerEvent e)
    {
        travelers.Remove(e.Traveler);
    }

    void OnDestroy()
    {
        try
        {
            EventManager.Instance.RemoveListener<RemoveTravelerEvent>(RemoveTraveler);
            EventManager.Instance.RemoveListener<RegisterTravelerEvent>(RegisterTraveler);
        }
        catch (Exception)
        {
            Debug.Log("Tried to destroy null object");
        }
    }
}
