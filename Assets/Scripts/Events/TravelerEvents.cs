﻿using System.Collections;
using System.Collections.Generic;
using Events;
using UnityEngine;

namespace TravelersEvents
{
    public class RegisterTravelerEvent : GameEvent
    {
        public GameObject Traveler { get; private set; }

        public RegisterTravelerEvent(GameObject traveler)
        {
            Traveler = traveler;
            args = new List<object> { Traveler };
        }
    }

    public class RemoveTravelerEvent : GameEvent
    {
        public GameObject Traveler { get; private set; }

        public RemoveTravelerEvent(GameObject traveler)
        {
            Traveler = traveler;
            args = new List<object> { Traveler };
        }
    }

}
