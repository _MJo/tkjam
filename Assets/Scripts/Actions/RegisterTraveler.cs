﻿using System.Collections;
using System.Collections.Generic;
using Managers;
using TravelersEvents;
using UnityEngine;

public class RegisterTraveler : MonoBehaviour
{
    void Start()
    {
        EventManager.Instance.InvokeEvent(new RegisterTravelerEvent(gameObject));
    }
}
