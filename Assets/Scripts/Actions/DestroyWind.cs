﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyWind : MonoBehaviour
{
    public float destroyTime = 2.0f;

    public void SetDestroy(float destroyTime)
    {
        Invoke("DestroyMe", destroyTime);
    }

    void DestroyMe()
    {
        Destroy(gameObject.GetComponent<ConstantForce>());
    }
}
